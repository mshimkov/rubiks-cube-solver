## Rubik's Cube Solver 

Implemented using Java. 

The project was a team effort between me and a classmate. 

<p float="left">
  <img src="./images/Screen Shot 2019-06-27 at 3.48.33 PM.png" width="400" />
  <img src="./images/Screen Shot 2019-06-27 at 3.48.40 PM.png" width="400" />
</p>

Developed in December 2017 as part of our Algorithm Analysis class. 